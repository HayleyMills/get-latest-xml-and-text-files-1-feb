
## get_latest_xml_and_text_files

*Note*: this workflow combines [create_new_export](https://gitlab.com/closer-cohorts1/archivist_export/create_new_export) and [download_text_files](https://gitlab.com/closer-cohorts1/archivist_export/download_text_files)


Loop over all prefix from Prefixes_to_export.txt
- click CREATE NEW EXPORT button
- download the newly created xml file
  - check if any xml files contain missing question/statement literal
  - clean text within xml
- Export text files from archivist
  - Question
    - QV (Question Variables) qv.txt, rename prefix.qvmapping.txt
    - TQ (Topic Questions) tq.txt, rename prefix.tqlinking.txt
  - Datasets
    - TV: tv.txt, rename: prefix.tvlinking.txt
    - DV: dv.txt, rename: prefix.dv.txt

